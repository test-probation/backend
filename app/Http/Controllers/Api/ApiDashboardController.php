<?php

namespace App\Http\Controllers\Api;

use App\Models\City;
use App\Models\Province;
use App\Models\Region;
use App\Models\RolePrivileges;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ApiDashboardController extends ApiResourcesController
{

    public function index(Request $request): JsonResponse
    {
        try {
            $regionId = $request->get('id_region');
            $provinceId = $request->get('id_province');

            $regionQuery = Region::query();
            $provinceQuery = Province::query();
            $cityQuery = City::query();

            if ($regionId) {
                $regionQuery->where('id', $regionId);
                $provinceQuery->where('id_region', $regionId);

                $cityQuery->whereIn('id_province', function ($query) use ($regionId) {
                    $query->select('id')
                        ->from('province')
                        ->where('id_region', $regionId);
                });
            }

            if ($provinceId) {
                $provinceQuery->where('id', $provinceId);
                $cityQuery->where('id_province', $provinceId);
            }

            $regionCount = $regionQuery->count();
            $provinceCount = $provinceQuery->count();
            $cityCount = $cityQuery->count();

            $entryData = [
                'countRegion' => $regionCount,
                'countProvince' => $provinceCount,
                'countCity' => $cityCount
            ];

            $allCount = $regionCount + $provinceCount + $cityCount;

            $data = [
                'result' => $entryData,
                'count' => $allCount,
            ];

            return $this->response->successResponse($data);
        } catch (Exception $error) {
            return $this->generalError($error);
        }
    }
}
