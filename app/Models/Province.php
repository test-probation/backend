<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Province extends Resources
{
  use HasFactory;

  protected $table = 'province';
  protected $fillable = ['name', 'id_region'];
  protected $searchable = ['name', 'id_region'];
  protected $reference = ['region','cities'];


  public function cities(): HasMany
  {
    return $this->hasMany(City::class, 'id_provinsi');
  }

  public function region(): BelongsTo
  {
    return $this->belongsTo(Region::class, 'id_region');
  }
}
