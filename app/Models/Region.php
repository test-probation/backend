<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Region extends Resources
{
  use HasFactory;

  protected $table = 'region';
  protected $fillable = ['name'];
  protected $searchable = ['name'];

  protected $reference = ['provinces'];

  public function provinces(): HasMany
  {
    return $this->hasMany(Province::class, 'id_region');
  }
}
